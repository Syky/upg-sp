import javax.swing.*;
import java.awt.*;

/**
 * Trida reprezentujici legendu grafu
 */
public class Legend extends JPanel {
    private ColorMap colorMap;

    public Legend(ColorMap colorMap) {
        this.colorMap = colorMap;
        this.setPreferredSize(new Dimension(230, 50));
        JFrame frame = new JFrame();
        frame.setTitle("Legenda");
        frame.add(this);
        frame.pack();
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        drawLegend(g2);
    }

    /**
     * Vykresleni legendy grafu
     * @param g2 graficky kontext
     */
    private void drawLegend(Graphics2D g2) {
        g2.setColor(new Color(213, 255, 215));
        g2.fill(new Rectangle(0,0,60,30));
        g2.setColor(Color.BLACK);
        g2.drawString("> " + this.colorMap.getIntervals()[7], 20, 20);

        g2.setColor(new Color(139, 203, 128));
        g2.fill(new Rectangle(60,0,60,30));
        g2.setColor(Color.BLACK);
        g2.drawString("> " + this.colorMap.getIntervals()[0], 80, 20);

        g2.setColor(new Color(100, 156, 52));
        g2.fill(new Rectangle(120,0,60,30));
        g2.setColor(Color.BLACK);
        g2.drawString("> " + this.colorMap.getIntervals()[1], 140, 20);

        g2.setColor(new Color(170, 190, 86));
        g2.fill(new Rectangle(180,0,60,30));
        g2.setColor(Color.BLACK);
        g2.drawString("> " + this.colorMap.getIntervals()[2], 200, 20);

        g2.setColor(new Color(207, 175, 108));
        g2.fill(new Rectangle(0,30,60,30));
        g2.setColor(Color.BLACK);
        g2.drawString("> " + this.colorMap.getIntervals()[3], 20, 50);

        g2.setColor(new Color(121, 95, 60));
        g2.fill(new Rectangle(60,30,60,30));
        g2.setColor(Color.BLACK);
        g2.drawString("> " + this.colorMap.getIntervals()[4], 80, 50);

        g2.setColor(new Color(93, 66, 48));
        g2.fill(new Rectangle(120,30,60,30));
        g2.setColor(Color.WHITE);
        g2.drawString("> " + this.colorMap.getIntervals()[5], 140, 50);

        g2.setColor(new Color(70, 50, 31));
        g2.fill(new Rectangle(180,30,60,30));
        g2.setColor(Color.WHITE);
        g2.drawString("> " + this.colorMap.getIntervals()[6], 200, 50);
    }
}
