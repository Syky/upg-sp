import waterflowsim.*;
import javax.swing.*;
import java.awt.*;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Trida reprezentujici graficke rozhrani.
 */
public class GUI {
    private double nextStep;
    private DrawingPanel panel;
    private Timer timer;
    private int count;
    private double simulationTime;


    public GUI(int scenarioIndex) {
        nextStep = 0.02;
        count = 0;
        simulationTime = 0;
        createGUI(scenarioIndex);
    }

    /**
     * Metoda, ktera zajisti vykresleni okna a pocatecni nastaveni, zaroven spusti scenar a nasledne ho "krokuje".
     * @param scenarioIndex index scenare
     */
    private void createGUI(int scenarioIndex) {
        Simulator.runScenario(scenarioIndex);
        JFrame frame = new JFrame();
        frame.setTitle("A17B0303P");
        frame.setLayout(new BorderLayout());

        panel = new DrawingPanel();
        JPanel bottomButtons = createBottomButtons();
        JPanel topButtons = createTopButtons();

        frame.add(panel, BorderLayout.CENTER);
        frame.add(topButtons, BorderLayout.NORTH);
        frame.add(bottomButtons, BorderLayout.SOUTH);
        frame.pack();

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        // Prekresleni okna
        startTimer();
    }

    /**
     * Metoda pro spusteni Timeru
     */
    private void startTimer() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Simulator.nextStep(nextStep);
                panel.repaint();

                simulationTime += nextStep;
                if (count % 25 == 0) {
                    double[] wl = new double[Simulator.getData().length];
                    for (int i = 0; i < Simulator.getData().length; i++) {
                        wl[i] = Simulator.getData()[i].getWaterLevel();
                    }
                    panel.graphRecords.add(new GraphRecord(simulationTime, wl));
                }
                count++;
            }
        },0,100);
    }

    /**
     * Metoda pro pozastaveni Timeru
     */
    private void pauseTimer() {
        timer.cancel();
    }


    private JPanel createTopButtons() {
        JPanel buttons = new JPanel();
        JButton legendBtn = new JButton("Legenda");
        JButton printBtn = new JButton("Tisk");
        JButton exportBtn = new JButton("Export");
        JButton svgBtn = new JButton("SVG");


        legendBtn.addActionListener(e -> {
            new Legend(panel.getColorMap());
        });

        printBtn.addActionListener(e -> {
            PrinterJob job = PrinterJob.getPrinterJob();
            if (job.printDialog()) {
                job.setPrintable(panel);
                try {
                    job.print();
                } catch (PrinterException ex) {
                    ex.printStackTrace();
                }
            }
        });

        exportBtn.addActionListener(e -> {
            JTextField width = new JTextField();
            JTextField height = new JTextField();

            Object[] fields = {
                    "Sirka: ", width,
                    "Vyska: ", height
            };

            JOptionPane.showConfirmDialog(null, fields, "Export", JOptionPane.OK_CANCEL_OPTION);

            if (!(width.getText().equals("") && height.getText().equals(""))) {
                panel.export(Integer.parseInt(width.getText()), Integer.parseInt(height.getText()));
                JOptionPane.showMessageDialog(null, "Vas obrazek byl vyexportovan do korenove slozky.");
            }
        });

        svgBtn.addActionListener(e-> {
            panel.SVG();
            JOptionPane.showMessageDialog(null, "Vas obrazek byl vyexportovan do korenove slozky.");
        });

        buttons.add(legendBtn);
        buttons.add(printBtn);
        buttons.add(exportBtn);
        buttons.add(svgBtn);

        buttons.setBackground(Color.WHITE);

        return buttons;
    }
    /**
     * Metoda, ktera vytvori Panel s tlacitky a priradi jim ovladani
     * @return
     */
    private JPanel createBottomButtons() {
        JPanel buttons = new JPanel();
        JButton fasterBtn = new JButton("Zrychlit");
        JButton slowerBtn = new JButton("Zpomalit");
        JButton stopBtn = new JButton("Stop");
        JButton continueBtn = new JButton("Pokracovat");
        continueBtn.setEnabled(false);

        fasterBtn.addActionListener(e -> {
            slowerBtn.setEnabled(true);
            if (nextStep + 0.005 >=  0.020) {
                fasterBtn.setEnabled(false);
            }
            nextStep += 0.005;
        });

        slowerBtn.addActionListener(e -> {
            fasterBtn.setEnabled(true);
            if (nextStep - 0.005 < 0) {
                slowerBtn.setEnabled(false);
                nextStep = 0.0001;
            }
            else {
                nextStep -= 0.005;
            }
        });

        stopBtn.addActionListener(e -> {
            pauseTimer();
            continueBtn.setEnabled(true);
            stopBtn.setEnabled(false);
        });

        continueBtn.addActionListener(e -> {
            startTimer();
            continueBtn.setEnabled(false);
            stopBtn.setEnabled(true);
        });

        buttons.add(slowerBtn);
        buttons.add(fasterBtn);
        buttons.add(stopBtn);
        buttons.add(continueBtn);

        buttons.setBackground(Color.WHITE);

        return buttons;
    }
}
