import org.jfree.graphics2d.svg.SVGGraphics2D;
import waterflowsim.Cell;
import waterflowsim.Simulator;
import waterflowsim.Vector2D;
import waterflowsim.WaterSourceUpdater;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Trida reprezentujici kreslici okno aplikace.
 */
public class DrawingPanel extends JPanel implements Printable {
    private int maxX, minX, maxY, minY;
    private double scale, offsetX, offsetY;
    private double pomerDx = Math.abs(Simulator.getDelta().x/Simulator.getDelta().y);
    private double pomerDy = Math.abs(Simulator.getDelta().y/Simulator.getDelta().x);
    private ColorMap colorMap;
    private int[][] pixels;
    private Graph graph, selectionGraph;
    private Point startDrag, endDrag;
    public ArrayList<GraphRecord> graphRecords;
    private double windowOffset;
    private boolean selection = false;


    private static final Color SKYBLUE = new Color(0,191,255);
    private static final Color LIGHT_GREY = new Color(122, 120, 133);
    private static final int LABEL_SIZE = 14;
    private static final int LABEL_OFFSET = 20;
    private static final int W_ARROW_SIZE = 1;
    private static final int B_ARROW_SIZE = 3;
    private static final int ARROW_LENGTH = 50;
    private static final int WINDOW_WIDTH = 640;
    private static final int WINDOW_HEIGHT = 640;


    public DrawingPanel() {
        this.setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        computeModelDimensions();
        pixels = getPixels(maxX, maxY);
        graphRecords = new ArrayList<>();
        createColorMap();
        handleMouseEvents();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        drawWaterFlowState(g2);

        if(graph != null) {
            graph.addGraphRecords(graphRecords);
        }

        if (selectionGraph != null) {
            selectionGraph.addSelGraphRecords(graphRecords);
        }

        if (startDrag != null && endDrag != null) {
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.50f));
            g2.setPaint(LIGHT_GREY);
            Shape r = drawSelectionBox(startDrag.x, startDrag.y, endDrag.x, endDrag.y);
            g2.fill(r);
        }
    }

    /**
     * Metoda, vykreslujici jak krajinu, tak vodni reprezentaci rek.
     * @param g graficky kontext
     */
    private void drawWaterFlowState(Graphics2D g) {
        drawTerrain(g);
        drawWaterLayer(g);
    }

    private void drawTerrain(Graphics2D g) {
        computeModel2WindowTransformation(this.getWidth(), this.getHeight());
        Cell[] cells = Simulator.getData();
        for (int x = 0; x < maxX; x++) {
            for (int y = 0; y < maxY; y++) {
                g.setColor(colorMap.getColor(cells[pixels[x][y]].getTerrainLevel()));
                Point2D windowPoint = model2window(new Point2D.Double(x, y));
                g.draw(new Rectangle2D.Double((int) windowPoint.getX(), (int) windowPoint.getY(), scale * pomerDx, scale* pomerDy));
            }
        }

    }

    /**
     * Metoda, ktera vykresli do okna aplikace vodni vrstu modrou barvou.
     * @param g graficky kontext
     */
    private void drawWaterLayer(Graphics2D g) {
        Cell[] cells = Simulator.getData();

        for (int x = 0; x < maxX; x++) {                //prochazeni pole po pixelech
            for (int y = 0; y < maxY; y++) {
                if (!cells[pixels[x][y]].isDry()) {     //pokud neni pixel suchy, pocita prevod a vykresli ctverec
                    g.setColor(SKYBLUE);
                    Point2D windowPoint = model2window(new Point2D.Double(x,y));
                    g.draw(new Rectangle2D.Double((int)windowPoint.getX(),(int)windowPoint.getY(),  scale * pomerDx,  scale * pomerDy));
                }
            }
        }
        drawWaterSources(g);
    }

    /**
     * Metoda, ktera prochazi vsechny vodni zdroje a nasledne se stara o jejich vykresleni do okna.
     * @param g graficky kontext
     */
    private void drawWaterSources(Graphics2D g) {
        WaterSourceUpdater[] sources = Simulator.getWaterSources();

        for (WaterSourceUpdater source : sources) {
            Vector2D<Double> vector = Simulator.getGradient(source.getIndex());
            Point2D position = model2window(getWaterSourcePosition(source.getIndex()));
            drawWaterFlowLabel(position, vector, source.getName(), g);
        }
    }

    /**
     * Metoda, ktera vykresli sipku s popiskem jako celek.
     * @param position  pozice pocatku sipky
     * @param dirFlow   vektor smeru sipky
     * @param name      jmeno zdroje
     * @param g         graficky kontext
     */
    private void drawWaterFlowLabel(Point2D position, Vector2D dirFlow, String name, Graphics2D g) {
        drawArrow(position, dirFlow, g, Color.BLACK, B_ARROW_SIZE);
        drawArrow(position, dirFlow, g, Color.WHITE, W_ARROW_SIZE);
        drawLabel(position, dirFlow, name, g);
    }

    /**
     * Metoda, ktera vykresli sipku v zavislosti na vektoru.
     * @param position  pozice pocatku sipky
     * @param dirFlow   vektor smeru sipky
     * @param g         graficky kontext
     * @param color     barva sipky
     * @param size      velikost sipky
     */
    private void drawArrow(Point2D position, Vector2D dirFlow, Graphics2D g, Color color, int size) {
        g.setColor(color);
        g.setStroke(new BasicStroke((int)(size * scale)));
        double x = position.getX();
        double y = position.getY();
        double vx = dirFlow.x.doubleValue();
        double vy = dirFlow.y.doubleValue();


        double vLength = Math.sqrt(vx*vx + vy*vy);
        double vNormX = vx / vLength;
        double vNormY = vy / vLength;

        double arrowLength = ARROW_LENGTH * scale;
        windowOffset = arrowLength;
        double vArrowX = vNormX * arrowLength;
        double vArrowY = vNormY * arrowLength;

        double kx = -vArrowY;
        double ky = vArrowX;

        kx *= 0.10;                             //natoceni sipky
        ky *= 0.10;

        double x2 = x - (int)vArrowX;           //obraceny vektor
        double y2 = y - (int)vArrowY;

        g.draw(new Line2D.Double(x, y, x2, y2));
        g.draw(new Line2D.Double(x2, y2, x2 + vArrowX*0.25 + kx, y2 + vArrowY*0.25 + ky));
        g.draw(new Line2D.Double(x2, y2, x2 + vArrowX*0.25 - kx, y2 + vArrowY*0.25 - ky));
    }

    /**
     * Metoda, ktera vykresluje popisek vodniho zdroje. Popisek je posunut ve smeru y a natocen podle
     * smeru sipky.
     * @param position  pozice pocatku popisku
     * @param dirFlow   vektor smeru sipky
     * @param name      jmeno
     * @param g         graficky kontext
     */
    private void drawLabel(Point2D position, Vector2D dirFlow, String name, Graphics2D g) {
        g.setColor(Color.BLACK);
        double x = position.getX();
        double y = position.getY();
        double vx = dirFlow.x.doubleValue();
        double vy = dirFlow.y.doubleValue();

        AffineTransform original = g.getTransform();
        AffineTransform at = g.getTransform();
        int textWidth = g.getFontMetrics(g.getFont()).stringWidth(name);
        if ((vx >= 0 && vy <= 0) ) {         //text je obracene
            at.rotate(vx, vy, x,y);
            at.translate(-textWidth,0);
        }

        else if (vx >= 0 && vy >= 0) {
            at.rotate(vx, vy, x,y);
            at.translate(-ARROW_LENGTH*scale,0);
        }
        else {
            at.rotate(-vx, -vy, x, y);
        }
        g.setTransform(at);
        g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, (int)(LABEL_SIZE * scale)));
        g.drawString(name,(int)x,(int)(y + LABEL_OFFSET * scale));
        g.setTransform(original);
    }

    /**
     * Metoda stanovi min/maxX a min/maxY modelu.
     */
    private void computeModelDimensions() {
        maxX = Simulator.getDimension().x;
        minX = 0;
        maxY = Simulator.getDimension().y;
        minY = 0;
    }

    /**
     * Metoda, která stanovi posun a promenou pro skalovani souranic modelu do souradnic okna. Zaroven je stanoven posun
     * takovy, aby se simulace vykreslovala ve stredu okna.
     * @param width velikost okna
     * @param height velikost okna
     */
    private void computeModel2WindowTransformation(int width, int height) {
        double modelWidth = maxX - minX;
        double modelHeight = maxY - minY;
        double margin = windowOffset;
        offsetX = margin;        //margin
        offsetY = margin;        //margin

        //double scaleX = (width*pomerDy) / modelWidth;
        //double scaleY = (height*pomerDx) / modelHeight;

        double scaleX = (width*pomerDy - margin * 2) / modelWidth;
        double scaleY = (height*pomerDx - margin * 2) / modelHeight;

        if(scaleX > scaleY) {
            scale = scaleY;
            //offsetX = (width - modelWidth * scale) / 2;
        }

        else {
            scale = scaleX;
            //offsetY = (height - modelHeight * scale) / 2;
        }
    }


    /**
     * Metoda, ktera prevede modelovy bod na bod k zobrazeni v okne aplikace.
     * @param m modelovy bod
     * @return bod v aplikaci
     */
    private Point2D model2window(Point2D m) {
        double x = m.getX() * scale * pomerDx + offsetX;
        double y = m.getY() * scale * pomerDy + offsetY;
        return new Point2D.Double(x,y);
    }

    private Point2D window2model(Point2D m) {
        double x = (m.getX() - offsetX) / (scale * pomerDx);
        double y = (m.getY() - offsetY) / (scale * pomerDy);
        return new Point2D.Double(x,y);
    }

    /**
     * Metoda vrati x a y z pixeloveho pole, ktere odpovida indexu.
     * @param index
     * @return  bod
     */
    private Point2D getWaterSourcePosition(int index) {
        Point2D point = new Point2D.Double();

        for (int x = 0; x < maxX; x++) {
            for (int y = 0; y < maxY; y++) {
                if (pixels[x][y] == index) {
                    point.setLocation(x,y);
                }
            }
        }
        return point;
    }

    /**
     * Metoda, ktera reprezentuje dvourozmerne pole pixelu modeloveho obrazku, serazene jako souradny system grafickoho
     * okna. Toto pole je nasledne oindexovano.
     * @param width     sirka modeloveho obrazku
     * @param height    vyska modeloveho obrazku
     * @return  pole pixelu
     */
    private int[][] getPixels(int width, int height) {
        int[][] pixels = new int[width][height];

        int index = 0;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                pixels[x][y] = index++;
            }
        }
        return pixels;
    }

    /**
     * Metoda vytvori barevnou mapu v zavislosti na vyskovych extremech.
     */
    private void createColorMap() {
        Cell[] cells = Simulator.getData();
        double maxHeight = 0;
        double minHeight = Double.MAX_VALUE;

        for (Cell c: cells) {
            maxHeight = Double.max(maxHeight, c.getTerrainLevel());
            minHeight = Double.min(minHeight, c.getTerrainLevel());
        }

        colorMap = new ColorMap(maxHeight,minHeight);
    }


    /**
     * Geter pro ziskani instance barevne mapy
     * @return  barevna mapa
     */
    public ColorMap getColorMap() {
        return colorMap;
    }


    /**
     * Metoda, ktera se stara o uzivatelskou obsluhu mysi
     */
    private void handleMouseEvents() {


        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Point2D point = new Point2D.Double(e.getX(), e.getY());
                int x = (int) window2model(point).getX();
                int y = (int) window2model(point).getY();

                if ((x < Simulator.getDimension().x && y < Simulator.getDimension().y) &&
                    (x > 0 && y >0)) {
                    graph = new Graph(pixels[x][y]);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                startDrag = new Point(e.getX(), e.getY());
                endDrag = startDrag;
                repaint();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (selection) {
                    if      ((startDrag.x < Simulator.getDimension().x && startDrag.y < Simulator.getDimension().y) &&
                            (startDrag.x > 0 && startDrag.y >0) &&
                            (endDrag.x < Simulator.getDimension().x && endDrag.y < Simulator.getDimension().y) &&
                            (endDrag.x > 0 && endDrag.y > 0)) {
                        selectionGraph = new Graph(selectedList(startDrag, endDrag));
                        selection = false;
                    }
                }
                startDrag = null;
                endDrag = null;
                repaint();
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        this.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                endDrag = new Point(e.getX(), e.getY());
                selection = true;
                repaint();
            }

            @Override
            public void mouseMoved(MouseEvent e) {

            }
        });
    }

    /**
     * Metoda vrati ArrayList vsech indexu, ktere spadaji pod oblast vybranou selection boxem od
     * uzivatele
     * @param start pocatecni bod selection boxu
     * @param end   koncovy bod selection boxu
     * @return
     */
    private ArrayList<Integer> selectedList(Point start, Point end) {
        ArrayList<Integer> selectedIndexes = new ArrayList<>();
        int x1 = (int) window2model(start).getX();
        int y1 = (int) window2model(start).getY();
        int x2 = (int) window2model(end).getX();
        int y2 = (int) window2model(end).getY();

        int minX = Math.min(x1,x2);
        int maxX = Math.max(x1,x2);
        int minY = Math.min(y1,y2);
        int maxY = Math.max(y1, y2);

        if ((x1 > 0 && x2 > 0 && y1 > 0 && y2 > 0) &&
            (x1 < Simulator.getDimension().x && x2 < Simulator.getDimension().x &&
             y1 < Simulator.getDimension().y && y2 < Simulator.getDimension().y)) {
            for (int i = minY; i <= maxY; i++) {
                for (int j = minX; j <= maxX; j++) {
                    selectedIndexes.add(pixels[j][i]);
                }
            }
        }
        return selectedIndexes;
    }

    /**
     * Metoda vykresli selection box.
     * @param x1 pocatecni bod
     * @param y1 pocatecni bod
     * @param x2 koncovy bod
     * @param y2 koncovy bod
     * @return
     */
    private Rectangle2D drawSelectionBox(int x1, int y1, int x2, int y2) {
        return new Rectangle2D.Double(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x1 - x2), Math.abs(y1 - y2));
    }

    /**
     * Metoda, ktera ulozi graficky kontext do obrazku o zvolenem rozliseni
     * @param width sirka vysledneho obrazku
     * @param height vyska vysledneho obrazku
     */
    public void export(int width, int height) {
        BufferedImage bImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics = bImage.createGraphics();

        double w = this.getWidth();
        double h = this.getHeight();

        double sx = width/w;
        double sy = height/h;

        graphics.scale(sx,sy);
        this.paintAll(graphics);
        try {
            if (ImageIO.write(bImage, "png", new File("./exported_img.png")))
            {
                System.out.println("png ulozeno");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void SVG() {
        SVGGraphics2D g2 = new SVGGraphics2D(this.getWidth(), this.getHeight());
        this.drawWaterFlowState(g2);

        try {
            FileWriter myWriter = new FileWriter("exported_svg.svg");
            myWriter.write(g2.getSVGElement());
            System.out.println("svg ulozeno");
            myWriter.close();
        } catch (IOException e) {
            System.out.println("Error pri zapisu SVG.");
            e.printStackTrace();
        }
    }

    /**
     * Metoda pro vytisknuti grafickeho kontextu
     * @param graphics graficyku kontext
     * @param pageFormat format stranky
     * @param pageIndex index stranyku
     * @return 0
     * @throws PrinterException
     */
    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        if (pageIndex > 0) {
            return NO_SUCH_PAGE;
        }

        Graphics2D g2 = (Graphics2D)graphics;

        g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

        drawWaterFlowState(g2);
        JOptionPane.showMessageDialog(null, "Vyckejte prosim, nez se obrazek vytiskne.");
        return 0;
    }
}
