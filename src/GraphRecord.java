/**
 * Trida, reprezentujici zaznam do grafu, ktery uchovava cas a vodni vysky vsech poli
 */
public class GraphRecord {
    double time;
    double[] waterLevels;

    public GraphRecord(double time, double[] waterLevels) {
        this.time = time;
        this.waterLevels = waterLevels;
    }

}

