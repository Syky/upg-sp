/**
 * Hlavni trida, ktera zkontroluje parametr zadany uzivatelem a spusti program.
 */
public class L01_SpusteniSimulatoru {

	/**
	 * Metoda, ktera vypise napovedu jak program spravne spustit.
	 */
	private static void printHelp() {
		System.out.println("Spatny pocet argumentu nebo neexistujici scenar! \nZvolte prosim cislo v rozsahu 0-6, " +
						   "nebo spustte program bez argumentu pro spusteni prvniho scenare.");
	}

	/**
	 * Metoda, ktera zkontroluje platnost parametru od uzivatele a pote spusti prislusny scenar.
	 * @param args	parametry od uzivatele
	 */
	private static void checkUsersInputAndRun(String[] args) {
		if (args.length == 0) {
			new GUI(0);
		}

		else if (args.length == 1) {
			String regex = "[0-6]";
			if (args[0].matches(regex)) {
				new GUI(Integer.parseInt(args[0]));
			}
			else {
				printHelp();
			}
		}
		else {
			printHelp();
		}
	}

	/**
	 * Hlavni metoda aplikace.
	 * @param args parametry od uzivatele
	 */
	public static void main(String[] args) {
		checkUsersInputAndRun(args);
	}
}
