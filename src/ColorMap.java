import java.awt.*;

/**
 * Trida reprezentujici barevnou mapu
 */
public class ColorMap {
    private double maxValue;
    private double minValue;
    private int intervals[];


    public ColorMap(double maxValue, double minValue) {
        this.maxValue = maxValue;
        this.minValue = minValue;
        calculateIntervals();
    }

    /**
     * Metoda spocte hranice jednotilivych intervalu a ulozi do pole
     */
    private void calculateIntervals() {
        intervals = new int[8];

        double range = maxValue - minValue;
        double intervalLenght = Math.ceil(range / 8.0);

        for (int i = 0; i < intervals.length-1; i++) {
            intervals[i] = (int) (intervalLenght * (i+1) + minValue);
        }
        intervals[7] = (int)minValue;
    }

    /**
     * Metoda pro ziskani barvy v zavislosti do jakeho intervalu spada
     * @param value hodnota vysky
     * @return  barva
     */
    public Color getColor(double value) {
        Color color = null;
        if (value > intervals[7]) {
            color = new Color(213, 255, 215);
        }
        if (value > intervals[0]) {
            color = new Color(139, 203, 128);
        }
        if (value > intervals[1]) {
            color = new Color(100, 156, 52);
        }
        if (value > intervals[2]) {
            color = new Color(170, 190, 86);
        }
        if (value > intervals[3]) {
            new Color(207, 175, 108);
        }
        if (value > intervals[4]) {
            color = new Color(121, 95, 60);
        }
        if (value > intervals[5]) {
            color = new Color(93, 66, 48);
        }
        if (value > intervals[6]) {
            color = new Color(70, 50, 31);
        }

        return color;
    }

    /**
     * Getter pro ziskani hranici intervalu
     * @return
     */
    public int[] getIntervals() {
        return intervals;
    }
}
