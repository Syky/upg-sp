import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.awt.*;
import java.util.*;

/**
 * Trida ktera reprezentuje graf
 */
public class Graph extends JPanel {
    private DefaultCategoryDataset dataset, selectionDataset;
    private int index;
    private ArrayList<Integer> indexes;
    private HashMap<Double, Double> timesAndAvgLevels;

    public Graph(int index) {
        this.index = index;
        JFrame graph = new JFrame();
        graph.setTitle("Graf pro jeden bod");
        graph.setLayout(new BorderLayout());

        ChartPanel chartPanel = new ChartPanel(createLineChart());
        graph.add(chartPanel, BorderLayout.CENTER);

        JButton printBtn = new JButton("Tisk");
        printBtn.addActionListener(e -> chartPanel.createChartPrintJob());
        graph.add(printBtn, BorderLayout.SOUTH);

        graph.pack();

        graph.setLocationRelativeTo(null);
        graph.setVisible(true);
    }

    public Graph(ArrayList<Integer> indexes) {
        this.indexes = indexes;
        timesAndAvgLevels = new HashMap<>();
        JFrame graph = new JFrame();
        graph.setTitle("Graf pro vice bodu");
        graph.setLayout(new BorderLayout());

        ChartPanel chartPanel = new ChartPanel(createSelLineChart());
        graph.add(chartPanel, BorderLayout.CENTER);

        JButton printBtn = new JButton("Tisk");
        printBtn.addActionListener(e -> chartPanel.createChartPrintJob());
        graph.add(printBtn, BorderLayout.SOUTH);

        graph.pack();

        graph.setLocationRelativeTo(null);
        graph.setVisible(true);

    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
    }

    /**
     * Metoda vytvori graf pro jeden bod.
     * @return graf
     */
    private JFreeChart createLineChart() {
        JFreeChart chart;
        dataset = new DefaultCategoryDataset();

        chart = ChartFactory.
                createLineChart("Graf zavislosti hladiny vody v case",
                        "Cas [s]",
                        "Vyska hladiny vody [m]",
                        dataset);
        return chart;
    }

    /**
     * Metoda vyvori graf ze prumerne vysky selectovanych bodu
     * @return graf
     */
    private JFreeChart createSelLineChart() {
        JFreeChart chart;
        selectionDataset = new DefaultCategoryDataset();

        chart = ChartFactory.
                createLineChart("Graf zavislosti hladiny vody v case",
                        "Cas [s]",
                        "Prumerna vyska hladiny vody [m]",
                        selectionDataset);
        return chart;
    }

    /**
     * Metoda prida vsechny zaznamy predane parametrem do grafu
     * @param graphRecords zaznam
     */
    public void addGraphRecords(ArrayList<GraphRecord> graphRecords) {
        ArrayList<GraphRecord> graphRecordsClone =  (ArrayList<GraphRecord>) graphRecords.clone();
        for (GraphRecord gr : graphRecordsClone) {
            String time = String.format("%.1f", gr.time);
            dataset.addValue(gr.waterLevels[index], "Vodni hladina", time);
        }
    }

    /**
     * Metoda prida vsechny zaznamy ze vyselectovane oblasti predane parametrem do grafu
     * @param graphRecords zaznamy
     */
    public void addSelGraphRecords(ArrayList<GraphRecord> graphRecords) {
        ArrayList<GraphRecord> graphRecordsClone = (ArrayList<GraphRecord>) graphRecords.clone();
        calculateAverageWaterLvl(graphRecordsClone);

        for (GraphRecord gr : graphRecordsClone) {
            String time = String.format("%.1f", gr.time);
            Double lvl = timesAndAvgLevels.get(gr.time);
            selectionDataset.addValue(lvl, "Vodni hladina", time);
        }

    }

    /**
     * Metoda vypocte prumernou vysku z vyselectovanych bodu, kde tece voda
     * @param graphRecords zaznamy
     */
    private void calculateAverageWaterLvl(ArrayList<GraphRecord> graphRecords) {
        ArrayList<GraphRecord> graphRecordsClone = (ArrayList<GraphRecord>) graphRecords.clone();
        double count = 0;
        double sum = 0;
        double time = 0;

        for (Integer i : indexes){
            for (GraphRecord gr : graphRecordsClone) {
                if (gr.waterLevels[i] != 0) {
                    sum += gr.waterLevels[i];
                    count++;
                    time = gr.time;
                }
            }
        }
        timesAndAvgLevels.put(time, sum/count);
    }

}

